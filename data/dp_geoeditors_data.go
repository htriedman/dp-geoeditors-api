package data

import (
	"context"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"

	"github.com/gocql/gocql"
)

type DPGeoeditorsDataInterface interface {
	ProcessDPGeoeditorsQuery(context context.Context, project, country_code, edit_range, month string, session *gocql.Session, rLogger *logger.RequestScopedLogger) gocql.Scanner
}

type DPGeoeditorsData struct {
}

func (s DPGeoeditorsData) ProcessDPGeoeditorsQuery(context context.Context, project, country_code, edit_range, month string, session *gocql.Session, rLogger *logger.RequestScopedLogger) gocql.Scanner {
	// Ensuring valid input for country, month, and edit_range
	country_code = country_code[0:2]
	month = month[0:7]
	if edit_range == "lo" {
		edit_range = "1 to 4"
	} else if edit_range == "med" {
		edit_range = "5 to 99"
	} else {
		edit_range = "100 or more"
	}
	query := `SELECT edit_range, epsilon, release_thresh, views FROM aqs.dp_geoeditors WHERE project = ? AND country_code = ? and edit_range = ? AND month = ?`
	scanner := session.Query(query, project, country_code, edit_range, month).WithContext(context).Iter().Scanner()
	return scanner
}
