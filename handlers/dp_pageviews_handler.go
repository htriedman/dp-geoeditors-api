package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"dp-geoeditors/configuration"
	"dp-geoeditors/logic"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

// DPGeoeditorsHandler is the HTTP handler for dp geoeditors endpoint requests.
type DPGeoeditorsHandler struct {
	Logger  *logger.Logger
	Session *gocql.Session
	Logic   logic.DPGeoeditorsLogicInterface
	Config  *configuration.Config
}

// API documentation
//
//	@summary		Get monthly geoeditors per per project, country, and editor activity level
//	@router			/dp-geoeditors/{project}/{country_code}/{activity_level}/{month} [get]
//	@description	Given a Wikimedia project, country, editor activity level, and month, returns the number of editors in that subgroup.
//	@param			project			path	string	true	"Domain of a Wikimedia project"				example(en.wikipedia.org)
//	@param			country_code	path	string	true	"Country to filter to"						example(US)
//	@param 			activity_level 	path 	string	true	"Activity level of editors"					example(lo, med, hi)
//	@param			month			path	string	true	"Month in YYYY-MM format"					example(02)
//	@produce		json
//	@success		200	{object}	entities.DPGeoeditorsResponse
//	@failure		400	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Bad request"
//	@failure		404	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Not found"
//	@failure		500	{object}	object{detail=string,method=string,status=int,title=string,uri=string}	"Internal server error"
func (s *DPGeoeditorsHandler) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	project := aqsassist.TrimProjectDomain(vars["project"])
	country_code := strings.ToUpper(vars["country_code"])
	activity_level := strings.ToLower(vars["activity_level"])
	month := strings.ToLower(vars["month"])
	year := strings.Split(month, "-")[0]
	month_short := strings.Split(month, "-")[1]

	reqUrl := r.URL.RequestURI()
	reqLogger := s.Logger.Request(r)

	var err error

	if !aqsassist.IsYear(year) || !aqsassist.IsMonth(month_short) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Month should be in YYYY-MM format between 2018 and present", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if aqsassist.GetTime(year, month, "01").Before(time.Date(2018, 1, 1, 0, 0, 0, 0, time.UTC)) {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Data not accessible via API prior to January 2018.", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	if !(activity_level == "lo" || activity_level == "med" || activity_level == "hi") {
		problemResp := aqsassist.CreateProblem(http.StatusBadRequest, "Activity level must be lo, med, or hi.", reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	c := context.Background()

	pbm, response := s.Logic.ProcessDPGeoeditorsLogic(c, reqUrl, project, country_code, activity_level, month, s.Session, reqLogger)
	if pbm != nil {
		(*pbm).WriteTo(w)
		return
	}

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		reqLogger.Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
		(*problemResp).WriteTo(w)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
