package entities

// DPGeoeditorsResponse represents a container for the DP Geoeditors resultset.
type DPGeoeditorsResponse struct {
	Item DPGeoeditors `json:"item"`
}

// DPPageviews represents one result from the DP pageviews resultset.
type DPGeoeditors struct {
	Project       string  `json:"project" example:"enwiki"`         // Wikimedia project domain
	CountryCode   string  `json:"country_code" example:"DE"`        // Country
	EditRange     string  `json:"edit_range" example:"lo, med, hi"` // Edit activity level range
	Month         string  `json:"month" example:"2023-12"`          // Month in YYYY-MM format
	Epsilon       float32 `json:"epsilon" example:"1.1"`            // DP parameter known as the privacy budget
	ReleaseThresh int     `json:"release_thresh" example:"90"`      // DP parameter indicating minimum data value below which data was not released
	Count         int     `json:"count" example:"1234"`             // Number of editors in editor class in country in project during YYYY-MM
}
