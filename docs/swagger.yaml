basePath: /metrics/
definitions:
  entities.DPGeoeditors:
    properties:
      count:
        description: Number of editors in editor class in country in project during
          YYYY-MM
        example: 1234
        type: integer
      country_code:
        description: Country
        example: DE
        type: string
      edit_range:
        description: Edit activity level range
        example: lo, med, hi
        type: string
      epsilon:
        description: DP parameter known as the privacy budget
        example: 1.1
        type: number
      month:
        description: Month in YYYY-MM format
        example: 2023-12
        type: string
      project:
        description: Wikimedia project domain
        example: enwiki
        type: string
      release_thresh:
        description: DP parameter indicating minimum data value below which data was
          not released
        example: 90
        type: integer
    type: object
  entities.DPGeoeditorsResponse:
    properties:
      item:
        $ref: '#/definitions/entities.DPGeoeditors'
    type: object
host: localhost:8080
info:
  contact: {}
  description: |
    *This page is a work in progress.*

    DP Geoeditors is a service that provides a public API developed and maintained by the Wikimedia Foundation. The API serves statistical
    data about number of editors that have reached a given level of activity in a given (project, country, month) tuple.

    Data provided by this API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
  termsOfService: https://wikimediafoundation.org/wiki/Terms_of_Use
  title: Wikimedia DP Geoeditors API
  version: DRAFT
paths:
  /dp-geoeditors/{project}/{country_code}/{activity_level}/{month}:
    get:
      description: Given a Wikimedia project, country, editor activity level, and
        month, returns the number of editors in that subgroup.
      parameters:
      - description: Domain of a Wikimedia project
        example: en.wikipedia.org
        in: path
        name: project
        required: true
        type: string
      - description: Country to filter to
        example: US
        in: path
        name: country_code
        required: true
        type: string
      - description: Activity level of editors
        example: lo, med, hi
        in: path
        name: activity_level
        required: true
        type: string
      - description: Month in YYYY-MM format
        example: "02"
        in: path
        name: month
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/entities.DPGeoeditorsResponse'
        "400":
          description: Bad request
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
        "404":
          description: Not found
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
        "500":
          description: Internal server error
          schema:
            properties:
              detail:
                type: string
              method:
                type: string
              status:
                type: integer
              title:
                type: string
              uri:
                type: string
            type: object
      summary: Get monthly geoeditors per per project, country, and editor activity
        level
schemes:
- http
swagger: "2.0"
