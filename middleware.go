package main

import (
	"io"
	"net/http"

	"gitlab.wikimedia.org/frankie/aqsassist"
)

// SetSecurityHeaders Add security headers
func SetSecurityHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, _ := io.ReadAll(r.Body)
		aqsassist.SetSecurityHeaders(w, body)
		next.ServeHTTP(w, r)
	})
}
