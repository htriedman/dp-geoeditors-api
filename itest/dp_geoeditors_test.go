package itest

// TODO: rewrite tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"dp-geoeditors/entities"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/dp-geoeditors"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func runQuery(t *testing.T, project, country, activity_level, month string) entities.DPGeoeditorsResponse {

	res, err := http.Get(testURL(fmt.Sprintf("%s/%s/%s/%s", project, country, activity_level, month)))

	require.NoError(t, err, "Invalid http request")

	require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")

	body, err := io.ReadAll(res.Body)
	require.NoError(t, err, "Unable to read response")

	n := entities.DPGeoeditorsResponse{}
	err = json.Unmarshal(body, &n)

	require.NoError(t, err, "Unable to unmarshal response body")
	return n
}

func TestDPGeoeditors(t *testing.T) {
	t.Run("should return 404 for an invalid route", func(t *testing.T) {
		// Leading slash should result in an invalid url
		res, err := http.Get(testURL("/enwiki/US/med/2023-01"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 200 for expected parameters", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/IN/lo/2023-06"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 400 when parameters are wrong", func(t *testing.T) {

		res, err := http.Get(testURL("wrong-project/wrong-country/invalid/1990-00"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("should return 400 when date is before 2018 January 1", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/IN/lo/2017-12"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when year is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/IN/lo/0000-06"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when month is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/IN/lo/2023-00"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 400 when activity_level is invalid", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/IN/invalid/2023-06"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")

	})

	t.Run("should return 404 for invalid route", func(t *testing.T) {

		res, err := http.Get(testURL("tewiki/.invalid/IN/lo/2023-06"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusNotFound, res.StatusCode, "Wrong status code")
	})

	t.Run("should return the correct data", func(t *testing.T) {

		n := runQuery(t, "tewiki", "IN", "lo", "2023-06")
		u := entities.DPGeoeditors{
			Project:       "tewiki",
			CountryCode:   "IN",
			EditRange:     "1 to 4",
			Month:         "2023-06",
			Epsilon:       1.1,
			ReleaseThresh: 8,
			Count:         347,
		}

		assert.Equal(t, n.Item, u, "Wrong contents")

	})

	t.Run("should return 405 for invalid HTTP verb", func(t *testing.T) {

		jsonBody := []byte(``)
		bodyReader := bytes.NewReader(jsonBody)

		res, err := http.Post(testURL("tewiki/IN/lo/2023-06"), "application/json; charset=utf-8", bodyReader)

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusMethodNotAllowed, res.StatusCode, "Wrong status code")
	})
}

func TestSecurityHeaders(t *testing.T) {
	response, error := http.Get(testURL("tewiki/IN/lo/2023-06"))
	require.NoError(t, error, "Invalid http request")
	t.Run("All security headers should be properly set", func(t *testing.T) {
		for headerName, headerValue := range aqsassist.HeadersToAdd {
			assert.Equal(t, headerValue, response.Header.Get(headerName), headerName+" should be set to "+headerValue)
		}
		assert.NotEmpty(t, response.Header.Get("Etag"), "Etag header should not be empty.")
	})
}
