package test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"testing"

	"dp-geoeditors/configuration"
	"dp-geoeditors/entities"
	"dp-geoeditors/handlers"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"gitlab.wikimedia.org/frankie/aqsassist"

	"net/http/httptest"

	"github.com/gocql/gocql"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"schneider.vip/problem"
)

const remoteAddr = "127.0.0.1:8080"

type MockDPGeoeditorsLogic struct {
	mock.Mock
}

func (m *MockDPGeoeditorsLogic) ProcessDPGeoeditorsLogic(context context.Context, reqUrl, project, country_code, activity_level, month string, session *gocql.Session, rLogger *log.RequestScopedLogger) (*problem.Problem, entities.DPGeoeditorsResponse) {
	args := m.Called(context, reqUrl, project, country_code, activity_level, month, session, rLogger)
	return args.Get(0).(*problem.Problem), args.Get(1).(entities.DPGeoeditorsResponse)
}

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/dp-geoeditors"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func TestDPGeoeditorsHandler_HandleHTTP(t *testing.T) {
	project := "enwiki"
	country_code := "US"
	month := "2023-01"
	activity_level := "med"
	var pbm = (*problem.Problem)(nil)

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockResponse := entities.DPGeoeditorsResponse{
		Item: entities.DPGeoeditors{
			Project:       project,
			CountryCode:   country_code,
			EditRange:     "1 to 4",
			Month:         month,
			Epsilon:       0.5,
			ReleaseThresh: 10,
			Count:         100,
		},
	}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/en.wikipedia.org/US/2023/01/01/123"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/enwiki/US/med/2023-01")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})

	ctx := context.Background()

	mockLogic.On("ProcessDPGeoeditorsLogic", ctx, req.URL.RequestURI(), aqsassist.TrimProjectDomain(project), country_code, activity_level, month, session, mockLogger.Request(req)).
		Return(pbm, mockResponse)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	var respData entities.DPGeoeditorsResponse
	err = json.Unmarshal(rec.Body.Bytes(), &respData)
	assert.NoError(t, err)
	assert.Equal(t, mockResponse, respData)

	mockLogic.AssertCalled(t, "ProcessDPGeoeditorsLogic", ctx, req.URL.RequestURI(), aqsassist.TrimProjectDomain(project), country_code, activity_level, month, session, mockLogger.Request(req))
}

func TestDPGeoeditorsHandler_HandleHTTP_ErrorFromLogic(t *testing.T) {
	project := "enwiki"
	country_code := "US"
	month := "2023-01"
	activity_level := "med"
	ctx := context.Background()

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/enwiki/US/med/2023-01"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/enwiki/US/med/2023-01")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})
	reqUri := req.URL.RequestURI()

	mockError := aqsassist.CreateProblem(http.StatusInternalServerError, "Query failed!", reqUri)

	mockLogic.On("ProcessDPGeoeditorsLogic", ctx, reqUri, aqsassist.TrimProjectDomain(project), country_code, activity_level, month, session, mockLogger.Request(req)).
		Return(mockError, entities.DPGeoeditorsResponse{}).Once()

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusInternalServerError, rec.Code)

	assert.Equal(t, mockError.JSONString(), rec.Body.String())

	mockLogic.AssertCalled(t, "ProcessDPGeoeditorsLogic", ctx, reqUri, aqsassist.TrimProjectDomain(project), country_code, activity_level, month, session, mockLogger.Request(req))
}

func TestDPGeoeditorsHandler_HandleHTTP_InvalidEarlyDate(t *testing.T) {
	project := "enwiki"
	country_code := "US"
	month := "2015-01"
	activity_level := "med"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/ennwiki/US/med/2015-01"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/ennwiki/US/med/2015-01")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Data not accessible via API prior to January 2018.", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}

func TestDPGeoeditorsHandler_HandleHTTP_InvalidYear(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	month := "invalid-01"
	activity_level := "med"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/enwiki/US/med/invalid-01"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/enwiki/US/med/invalid-01")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Month should be in YYYY-MM format between 2018 and present", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}

func TestDPGeoeditorsHandler_HandleHTTP_InvalidMonth(t *testing.T) {
	project := "en.wikipedia.org"
	country_code := "US"
	month := "2023-invalid"
	activity_level := "med"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/enwiki/US/med/2023-invalid"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/enwiki/US/med/2023-invalid")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Month should be in YYYY-MM format between 2018 and present", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())
}

func TestDPGeoeditorsHandler_HandleHTTP_Invalid_ActivityLevel(t *testing.T) {
	project := "enwiki"
	country_code := "US"
	month := "2023-01"
	activity_level := "invalid"

	mockLogger := &log.Logger{}
	session := &gocql.Session{}

	mockLogic := new(MockDPGeoeditorsLogic)

	handler := &handlers.DPGeoeditorsHandler{
		Logger:  mockLogger,
		Session: session,
		Logic:   mockLogic,
		Config:  &configuration.Config{},
	}

	req, err := http.NewRequest("GET", testURL("/enwiki/US/invalid/2023-01"), nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = remoteAddr
	req.URL.RawPath = testURL("/enwiki/US/invalid/2023-01")
	req = mux.SetURLVars(req, map[string]string{
		"project":        project,
		"country_code":   country_code,
		"activity_level": activity_level,
		"month":          month,
	})
	reqUri := req.URL.RequestURI()

	error := aqsassist.CreateProblem(http.StatusBadRequest, "Activity level must be lo, med, or hi.", reqUri)

	rec := httptest.NewRecorder()
	handler.HandleHTTP(rec, req)

	assert.Equal(t, http.StatusBadRequest, rec.Code)
	assert.Equal(t, error.JSONString(), rec.Body.String())

}
