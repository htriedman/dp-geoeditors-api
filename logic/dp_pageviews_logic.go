package logic

import (
	"context"
	"dp-geoeditors/data"
	"dp-geoeditors/entities"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"gitlab.wikimedia.org/frankie/aqsassist"
	"schneider.vip/problem"
)

type DPGeoeditorsLogicInterface interface {
	ProcessDPGeoeditorsLogic(context context.Context, reqUrl, project, country_code, activity_level, month string, session *gocql.Session, rLogger *logger.RequestScopedLogger) (*problem.Problem, entities.DPGeoeditorsResponse)
}

type DPGeoeditorsLogic struct {
	Data data.DPGeoeditorsDataInterface
}

func NewDPGeoeditorsLogic(data data.DPGeoeditorsDataInterface) DPGeoeditorsLogic {
	return DPGeoeditorsLogic{Data: data}
}

func (s DPGeoeditorsLogic) ProcessDPGeoeditorsLogic(context context.Context, reqUrl, project, country_code, activity_level, month string, session *gocql.Session, rLogger *logger.RequestScopedLogger) (*problem.Problem, entities.DPGeoeditorsResponse) {
	var err error
	var problemData *problem.Problem
	var item entities.DPGeoeditors
	var response = entities.DPGeoeditorsResponse{Item: item}
	var edit_range string
	var epsilon float32
	var release_thresh, count int
	scanner := s.Data.ProcessDPGeoeditorsQuery(context, project, country_code, activity_level, month, session, rLogger)
	for scanner.Next() {
		if err = scanner.Scan(&edit_range, &epsilon, &release_thresh, &count); err != nil {
			if err.Error() == "not found" {
				problemResp := aqsassist.CreateProblem(http.StatusNotFound, "The date you used is valid, but we either do "+
					"not have or do not report data for that date or for that country. "+
					"Please consult the API docs and the Country Protection List Policy for more information.", reqUrl)
				return problemResp, entities.DPGeoeditorsResponse{}
			} else {
				rLogger.Log(logger.ERROR, "Query failed: %s", err)
				problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
				return problemResp, entities.DPGeoeditorsResponse{}
			}
		}
		response.Item = entities.DPGeoeditors{
			Project:       project,
			CountryCode:   country_code,
			EditRange:     edit_range,
			Month:         month,
			Epsilon:       epsilon,
			ReleaseThresh: release_thresh,
			Count:         count,
		}
	}

	err_str := "The date you used is valid, but we either do not have or do not report data for that date or for that country. Please consult the API docs and the Country Protection List Policy for more information."
	if response.Item.Project == "" {
		problemResp := aqsassist.CreateProblem(http.StatusNotFound, err_str, reqUrl)
		return problemResp, entities.DPGeoeditorsResponse{}
	}

	if err := scanner.Err(); err != nil {
		rLogger.Log(logger.ERROR, "Error querying database: %s", err)
		problemResp := aqsassist.CreateProblem(http.StatusInternalServerError, err.Error(), reqUrl)
		return problemResp, entities.DPGeoeditorsResponse{}
	}
	return problemData, response
}
