*This page is a work in progress.*

DP Geoeditors is a service that provides a public API developed and maintained by the Wikimedia Foundation. The API serves statistical
data about number of editors that have reached a given level of activity in a given (project, country, month) tuple.

Data provided by this API is available under the [CC0 1.0 license](https://creativecommons.org/publicdomain/zero/1.0/).
