package test

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"testing"

	"dp-geoeditors/entities"
	"dp-geoeditors/logic"

	log "gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

type MockDPGeoeditorsData struct {
	mock.Mock
}

type MockScanner struct {
	mock.Mock
	data  [][]interface{}
	index int
}

func (m *MockScanner) SetData(data [][]interface{}) {
	m.data = data
	m.index = -1
}

func (m *MockScanner) Scan(dest ...interface{}) error {
	if m.index >= len(m.data) {
		return io.EOF
	}
	row := m.data[m.index]
	if len(dest) != len(row) {
		return fmt.Errorf("not found")
	}
	for i, val := range row {
		if val == nil {
			continue
		}
		valType := reflect.TypeOf(val)
		destType := reflect.TypeOf(dest[i]).Elem()
		if !valType.ConvertibleTo(destType) {
			return fmt.Errorf("cannot convert value of type %s to type %s", valType, destType)
		}
		destValue := reflect.ValueOf(dest[i]).Elem()
		destValue.Set(reflect.ValueOf(val).Convert(destType))
	}
	return nil

}

func (m *MockScanner) Err() error {
	// return an error if there was an error scanning values,
	// or return nil if no error occurred
	return nil
}

func (m *MockScanner) Next() bool {
	m.index++
	return m.index < len(m.data)

}

func (m *MockDPGeoeditorsData) ProcessDPGeoeditorsQuery(context context.Context, project, country_code, activity_level, month string, session *gocql.Session, rLogger *log.RequestScopedLogger) gocql.Scanner {
	args := m.Called(context, project, country_code, activity_level, month, session, rLogger)
	return args.Get(0).(gocql.Scanner)
}

func TestProcessDPGeoeditorsLogic_Success(t *testing.T) {
	// setup mock data
	mockData := &MockDPGeoeditorsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	expectedResponse := entities.DPGeoeditorsResponse{
		Item: entities.DPGeoeditors{
			Project:       "testProject",
			CountryCode:   "testCountry",
			EditRange:     "1 to 4",
			Month:         "YYYY-MM",
			Epsilon:       0.5,
			ReleaseThresh: 100,
			Count:         123,
		},
	}

	// setup expectations for ProcessDPGeoeditorsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"1 to 4", 0.5, 100, 123},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessDPGeoeditorsQuery", mock.Anything, "testProject", "testCountry", "1 to 4", "YYYY-MM", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPGeoeditorsLogic(mockData)

	// call ProcessDPGeoeditorsLogic
	_, actualResponse := logic.ProcessDPGeoeditorsLogic(context.Background(), "", "testProject", "testCountry", "1 to 4", "YYYY-MM", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, expectedResponse, actualResponse)
}

func TestProcessDPGeoeditorsLogic_NoData(t *testing.T) {
	// setup mock data
	mockData := &MockDPGeoeditorsData{}
	session := &gocql.Session{}
	rLogger := &log.RequestScopedLogger{}

	// setup expectations for ProcessDPGeoeditorsQuery call
	mockScanner := new(MockScanner)
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)
	mockData.On("ProcessDPGeoeditorsQuery", mock.Anything, "testProject", "testCountry", "testActivityLevel", "YYYY-MM", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPGeoeditorsLogic(mockData)

	// call ProcessDPGeoeditorsLogic
	_, actualResponse := logic.ProcessDPGeoeditorsLogic(context.Background(), "", "testProject", "testCountry", "testActivityLevel", "YYYY-MM", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, "", actualResponse.Item.Project)
}

func TestProcessDPGeoeditorsLogic_QueryError(t *testing.T) {
	// setup mock data
	mockData := &MockDPGeoeditorsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080/metrics/dp-geoeditors/", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)

	// setup expectations for ProcessDPGeoeditorsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"1 to 4", 0.5, 100},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockScanner.On("Err").Return(nil)
	mockScanner.On("Close").Return(nil)

	mockData.On("ProcessDPGeoeditorsQuery", mock.Anything, "testProject", "testCountry", "1 to 4", "YYYY-MM", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPGeoeditorsLogic(mockData)

	// call ProcessDPGeoeditorsLogic
	pbmResp, actualResponse := logic.ProcessDPGeoeditorsLogic(context.Background(), "", "testProject", "testCountry", "1 to 4", "YYYY-MM", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, 0, actualResponse.Item.Count)
	assert.NotEmpty(t, pbmResp.Error())
}

func TestProcessDPGeoeditorsLogic_NotFound(t *testing.T) {
	// setup mock data
	mockData := &MockDPGeoeditorsData{}
	session := &gocql.Session{}
	req, err := http.NewRequest("GET", "localhost:8080/metrics/dp-geoeditors/", nil)
	if err != nil {
		t.Fatalf("Failed to create test request: %v", err)
	}
	req.RemoteAddr = "127.0.0.1:8080"
	logger, _ := log.NewLogger(os.Stdout, "", "error")
	rLogger := logger.Request(req)

	// setup expectations for ProcessDPGeoeditorsQuery call
	mockScanner := new(MockScanner)
	mockScanner.SetData([][]interface{}{
		{"1 to 4", 0.5, 100},
	})
	mockScanner.On("Next").Return(true).Once()
	mockScanner.On("Next").Return(false)
	mockScanner.On("Scan", mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("not found"))

	expectedProblem := aqsassist.CreateProblem(http.StatusNotFound, "The date you used is valid, but we either do not have or "+
		"do not report data for that date or for that country. "+
		"Please consult the API docs and the Country Protection List Policy for more information.", req.RequestURI)

	mockData.On("ProcessDPGeoeditorsQuery", mock.Anything, "testProject", "testCountry", "testActivityLevel", "YYYY-MM", session, rLogger).Return(mockScanner)

	// instantiate logic with mock data
	logic := logic.NewDPGeoeditorsLogic(mockData)

	// call ProcessDPGeoeditorsLogic
	pbmResp, actualResponse := logic.ProcessDPGeoeditorsLogic(context.Background(), "", "testProject", "testCountry", "testActivityLevel", "YYYY-MM", session, rLogger)

	// assert response matches expected value
	assert.Equal(t, "", actualResponse.Item.Project)
	assert.Equal(t, expectedProblem.Error(), pbmResp.Error())
}
